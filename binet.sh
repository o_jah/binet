#!/bin/bash

while [[ $# > 0 ]]
do
key="$1"
url="hdfs://labanlyhdfscluster/twitter/TTFHRawPost/::"
case $key in
    -w|--window)
    WINDOW="$2"
    shift # past argument
    ;;
    -a|--alpha)
    ALPHA="$2"
    shift
    ;;
    -g|--gamma)
    GAMMA="$2"
    shift
    ;;
    -k)
    K="$2"
    shift
    ;;
    -o|--outfile)
    OUTFILE="$2"
    shift
    ;;
    -m|--method)
    METHOD="$2"
    ;;
    *)

    ;;
esac
shift
done
echo ${INFILE}

java -Xmx8g -jar target/binet-jar-with-dependencies.jar -t ${url}${WINDOW}

# Spark
#
#/opt/spark/bin/spark-submit \
#    --class com.sysomos.bigram.nn.BigramNNModel \--master yarn-client \
#    --num-executors 85\
#    --driver-memory 7g \
#    --executor-memory 6g \
#    target/binet-jar-with-dependencies.jar -t ${url}${WINDOW} \
