package com.sysomos.bigram.nn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import com.beust.jcommander.JCommander;
import com.sysomos.bigram.nn.BigramNNModel.ModelParameters;
import com.sysomos.bigram.nn.exceptions.BigramNNModelException;

public class BigramNNModelTest {

	private BigramNNModel binet;
	private ModelParameters params;
	private static final List<String> ARG_QUERIES = new ArrayList<String>();

	static {
		ARG_QUERIES.addAll(Arrays.asList(new String[] { "-t", "" }));
	}

	private static final String ARGS[] = ARG_QUERIES
			.toArray(new String[ARG_QUERIES.size()]);

	@Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	@Before
	public void setUp() {
		params = new ModelParameters();
		new JCommander(params, ARGS).parse();
		binet = new BigramNNModel();
	}

	// @Test
	public void buildTest() throws BigramNNModelException {
		binet.build(params);
	}

	@After
	public void teardown() {
		binet = null;
	}
}
