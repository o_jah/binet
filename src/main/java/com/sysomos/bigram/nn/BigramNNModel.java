package com.sysomos.bigram.nn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.sysomos.base.filter.rule.BaseRule;
import com.sysomos.bigram.nn.exceptions.BigramNNModelException;
import com.sysomos.text.filter.BaseTweetItemFilter;
import com.sysomos.text.filter.RulePipeline;
import com.sysomos.text.filter.TweetItemFilterFactory;
import com.sysomos.text.filter.rule.IgnoreIfEmptyRule;
import com.sysomos.text.filter.rule.IgnoreIfIsMentionRule;
import com.sysomos.text.filter.rule.IgnoreIfIsRetweetRule;
import com.sysomos.text.filter.rule.IgnoreIfLanguageNotInRule;
import com.sysomos.text.filter.rule.IgnoreIfLengthLessThanRule;
import com.sysomos.text.sentenceiterator.HadoopTweetItemIterator;
import com.sysomos.text.sentenceiterator.SentenceIterator;
import com.sysomos.text.tokenizer.factory.DefaultTokenizerFactory;
import com.sysomos.word2vec.Word2Vec;
import com.sysomos.word2vec.loader.WordVectorSerializer;

public class BigramNNModel {

	private static final Logger LOG = LoggerFactory
			.getLogger(BigramNNModel.class);
	private static List<Class<? extends BaseRule>> rules;

	static {
		rules = new ArrayList<Class<? extends BaseRule>>();

		rules.add(IgnoreIfEmptyRule.class);
		rules.add(IgnoreIfIsRetweetRule.class);
		rules.add(IgnoreIfIsMentionRule.class);
		rules.add(IgnoreIfLengthLessThanRule.class);
	}

	public static class SentenceIteratorConverter
			implements IStringConverter<SentenceIterator> {

		@Override
		public HadoopTweetItemIterator convert(String path) {
			if (StringUtils.isEmpty(path))
				return null;
			String tokens[] = path.split("::");
			if (tokens == null || tokens.length < 2)
				return null;
			String url = tokens[0].trim();
			int window = 30;
			try {
				window = Integer.parseInt(tokens[1].trim());
			} catch (NumberFormatException e) {
				LOG.warn("Cannot extract window ... Defaulting to 30 days");
			}
//			RulePipeline pipeline = new RulePipeline(rules);
//			pipeline.ruleInstances.add(new IgnoreIfLanguageNotInRule(Arrays.asList(
//							new String[] { "en", "fr", "ar", "es", "pt", "et", "fa", "it" })));
			BaseTweetItemFilter filter = TweetItemFilterFactory
					.create(new RulePipeline(rules));
			HadoopTweetItemIterator iter = new HadoopTweetItemIterator(url,
					window, filter);
			return iter;
		}
	}

	public static class PositiveInteger implements IParameterValidator {

		@Override
		public void validate(String name, String value)
				throws ParameterException {
			int n = Integer.parseInt(value);
			if (n <= 0) {
				throw new ParameterException("Parameter " + name
						+ " should be positive (found " + value + ")");
			}
		}
	}
	
	public static class BoundedValue implements IParameterValidator {
		@Override
		public void validate(String name, String value) {
			float val = Float.valueOf(value);
			if (val < 0 || val > 1) {
				throw new ParameterException("Parameter " + name
						+ " should be in [0, 1] (found " + value + ")");
			}
		}
	}

	public static class ModelParameters {
		@Parameter(names = { "-training",
				"-t" }, required = true, description = "HDFS url to retrieve Twitter feeds and training window."
						+ " Example <hdfs://labanlyhdfscluster/twitter/TTFHRawPost/>::<30>", converter = SentenceIteratorConverter.class)
		private SentenceIterator sentenceIter = null;
		@Parameter(names = { "-size",
				"-s" }, required = false, description = "Size of the vocabulary to consider when training the bigram "
						+ "neural net model ", validateWith = PositiveInteger.class)
		private int top = 50000;
		@Parameter(names = { "-b",
				"-batch" }, required = false, description = "", validateWith = PositiveInteger.class)
		private int batchSize = 1000;
		@Parameter(names = { "-w",
				"-wordFreq" }, required = false, description = "Occurrence count of a word above which the word will "
						+ "be included in the vocabulary", validateWith = PositiveInteger.class)
		private int minWordFreq = 20;
		@Parameter(names = { "-lr",
				"-lRate" }, required = false, validateWith = BoundedValue.class, description = "Learning rate for "
						+ "stochastic gradient descent")
		private double learningRate = 0.025;
		@Parameter(names = { "-mlr",
				"-mLRate" }, required = false, validateWith = BoundedValue.class, description = "Minimum Learning rate "
						+ "for stochastic gradient descent")
		private double minLearningRate = 1e-2;
		@Parameter(names = { "-h",
				"-hUnit" }, required = false, validateWith = PositiveInteger.class, description = "Number of hidden "
						+ "units")
		private int hiddenUnit = 120;
	}

	public void build(final ModelParameters params)
			throws BigramNNModelException {

		if (params.sentenceIter == null)
			throw new BigramNNModelException("File Directory not Found.");	
	
		Word2Vec word2Vec = new Word2Vec.Builder()
				.useAdaGrad(false)
				.layerSize(params.hiddenUnit)
				.learningRate(params.learningRate)
				.minLearningRate(params.minLearningRate)
				.negativeSample(0)
				.iterate(params.sentenceIter)
				.tokenizerFactory(new DefaultTokenizerFactory())
				.minWordFrequency(params.minWordFreq)
				.saveVocab(true)
				.build();
		
		System.out.println("*** Word2Vec.fit: start " + new DateTime());
		try {
			word2Vec.fit();
			LOG.info("Save vectors....");
			WordVectorSerializer.writeWordVectors(word2Vec, "words.txt");
		} catch (IOException e) {
			throw new BigramNNModelException(e);
		} finally {
			System.out.println("*** Word2Vec.fit: end " + new DateTime());
		}
	}
	
	public static void main(String args[]) {
		ModelParameters params = new ModelParameters();
		new JCommander(params, args).parse();
		BigramNNModel model = new BigramNNModel();
		try {
			model.build(params);
		} catch (BigramNNModelException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}
}
