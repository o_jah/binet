package com.sysomos.word2vec;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sysomos.text.sentenceiterator.SentenceIterator;
import com.sysomos.text.utils.Pair;
import com.sysomos.text.worditerator.LineWordIterator;
import com.sysomos.word2vec.utils.SparkUtils;
import com.sysomos.word2vec.wordstore.VocabCache;

public class VocabWordTarget {

	private static final Logger LOG = LoggerFactory
			.getLogger(VocabWordTarget.class);
	private static final int BATCH_SIZE = 1000;

	public static void setVocabWordTargets(final VocabCache cache,
			final SentenceIterator sentenceIter) {
		if (sentenceIter != null && !sentenceIter.hasNext())
			sentenceIter.reset();
		if (sentenceIter == null || cache == null)
			return;

		List<Pair<String, String>> bigrams = new ArrayList<Pair<String, String>>();
		populate(sentenceIter, bigrams);

		Map<Pair<String, String>, Integer> counts = SparkUtils.count(bigrams);
		for (int i = 0; i < cache.numWords(); i++) {
			String word = cache.wordAtIndex(i), targetWord = null;
			int max = Integer.MIN_VALUE;
			boolean matchFound = false;
			for (Map.Entry<Pair<String, String>, Integer> e : counts
					.entrySet()) {
				if (e.getKey().getFirst() == word) {
					if (max < e.getValue()) {
						max = e.getValue();
						targetWord = e.getKey().getSecond();
					}
					if (!matchFound)
						matchFound = true;
				} else if (matchFound)
					break;
			}
			cache.wordFor(word).setTarget(cache.wordFor(targetWord));
		}
	}

	private static void populate(SentenceIterator sentenceIter,
			List<Pair<String, String>> bigrams) {
		ExecutorService service = Executors.newSingleThreadExecutor();
		List<Future<List<Pair<String, String>>>> futures;
		futures = new ArrayList<Future<List<Pair<String, String>>>>();

		List<String> batch = new ArrayList<String>();
		while (sentenceIter.hasNext()) {
			if (batch.size() < BATCH_SIZE) {
				batch.add(sentenceIter.nextSentence());
				continue;
			}
			futures.add(doIteration(batch, service));
			batch = new ArrayList<String>();
		}
		if (!CollectionUtils.isEmpty(batch))
			futures.add(doIteration(batch, service));
		service.shutdown();

		for (int i = 0; i < futures.size(); i++) {
			Future<List<Pair<String, String>>> task = futures.get(i);
			try {
				List<Pair<String, String>> pairs = task.get();
				if (CollectionUtils.isEmpty(pairs))
					continue;
				bigrams.addAll(pairs);
			} catch (InterruptedException e) {
				LOG.error(e.getLocalizedMessage(), e);
			} catch (ExecutionException e) {
				LOG.error(e.getLocalizedMessage(), e);
			} catch (IllegalArgumentException e) {
				LOG.error(e.getLocalizedMessage(), e);
			}
		}
	}

	private static Future<List<Pair<String, String>>> doIteration(
			final List<String> sentences, ExecutorService service) {
		if (CollectionUtils.isEmpty(sentences))
			throw new IllegalArgumentException(
					"List is null or empty...Skipping.");

		final List<Pair<String, String>> pairs = new ArrayList<Pair<String, String>>();
		Callable<List<Pair<String, String>>> callable;
		callable = new Callable<List<Pair<String, String>>>() {

			@Override
			public List<Pair<String, String>> call() throws Exception {
				for (String str : sentences) {
					if (StringUtils.isEmpty(str))
						continue;
					LineWordIterator lineIterator = new LineWordIterator(str,
							true);
					if (!lineIterator.hasNext())
						continue;
					String word = lineIterator.nextWord(), next = null;
					while (lineIterator.hasNext()) {
						next = lineIterator.nextWord();
						if (StringUtils.isEmpty(next))
							continue;
						pairs.add(new Pair<String, String>(word, next));
						word = next;
					}
				}
				return pairs;
			}
		};
		return service.submit(callable);
	}
}
