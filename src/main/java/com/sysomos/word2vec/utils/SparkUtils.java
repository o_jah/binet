package com.sysomos.word2vec.utils;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class SparkUtils {

	private static String APP_NAME = "BigramNNModel.WordCount";
	private static SparkConf sparkConf = new SparkConf().setAppName(APP_NAME);
	private static JavaSparkContext sparkContext = new JavaSparkContext(
			sparkConf);

	private static <T> JavaPairRDD<T, Integer> reduce(final List<T> contents) {
		JavaRDD<T> words = sparkContext.parallelize(contents);
		JavaPairRDD<T, Integer> ones = words
				.mapToPair(new PairFunction<T, T, Integer>() {
					private static final long serialVersionUID = -2L;

					@Override
					public Tuple2<T, Integer> call(T s) {
						return new Tuple2<T, Integer>(s, 1);
					}
				});
		JavaPairRDD<T, Integer> counts = ones
				.reduceByKey(new Function2<Integer, Integer, Integer>() {
					private static final long serialVersionUID = -3L;

					@Override
					public Integer call(Integer i1, Integer i2) {
						int tf = i1 + i2;
						return tf;
					}
				});
		counts.sortByKey(true);
		return counts;
	}

	public static <T> Map<T, Integer> count(final List<T> contents) {
		JavaPairRDD<T, Integer> counts = reduce(contents);
		if (counts == null)
			return null;
		counts.sortByKey(true);
		return counts.collectAsMap();
	}

	public static <T> Map<T, Integer> count(final List<T> contents, int top) {
		JavaPairRDD<T, Integer> counts = reduce(contents);
		if (counts == null)
			return null;
		List<Tuple2<T, Integer>> tuples = counts.takeOrdered(top,
				new Comparator<Tuple2<T, Integer>>() {

					@Override
					public int compare(Tuple2<T, Integer> o1,
							Tuple2<T, Integer> o2) {
						return o1._2 < o2._2 ? 0 : 1;
					}
				});
		Map<T, Integer> wordFreq = new HashMap<T, Integer>();
		for (Tuple2<T, Integer> tuple : tuples) {
			if (tuple == null)
				continue;
			wordFreq.put(tuple._1, tuple._2);
		}
		return wordFreq;
	}
}
