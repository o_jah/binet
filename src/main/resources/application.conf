akka {

	#extensions = ["akka.contrib.pattern.ClusterReceptionistExtension","akka.contrib.pattern.DistributedPubSubExtension"]

	# Akka version, checked against the runtime version of Akka.
  	version = "2.3.4"
  	
  	# Home directory of Akka, modules in the deploy directory will be loaded
  	home = ""

	# Loggers to register at boot time (akka.event.Logging$DefaultLogger logs
  	# to STDOUT)
  	loggers = ["akka.event.Logging$DefaultLogger"]
  	
  	logging-filter = "akka.event.DefaultLoggingFilter"
  	
  	loglevel = "INFO"

  	stdout-loglevel = "WARNING"

  	logger-startup-timeout = 5s

  	log-config-on-start = off
  	log-dead-letters = 10
  	log-dead-letters-during-shutdown = on
  	
  	extensions = []
  	
  	daemonic = off
  	jvm-exit-on-fatal-error = on

	actor {
		provider = "akka.actor.LocalActorRefProvider" 	# "akka.cluster.ClusterActorRefProvider"
		serialize-messages = off
		serialize-creators = off
		guardian-supervisor-strategy = "akka.actor.DefaultSupervisorStrategy"
		creation-timeout = 20s
		serialize-messages = off
		serialize-creators = off
		
		#dispatch  {
		#	bounded-control-aware-message-queue-semantics = "akka.actor.mailbox.bounded-control-aware-queue-based"
		#}
		
		
		log-dead-letters = 10
		
		# Timeout for send operations to top-level actors which are in the process
    	# of being started. This is only relevant if using a bounded mailbox or the
    	# CallingThreadDispatcher for a top-level actor.
    	unstarted-push-timeout = 10s
    	
    	
    	default-mailbox {
      		mailbox-type = "akka.dispatch.UnboundedMailbox"
 		
      		mailbox-capacity = 1000
 		
      		mailbox-push-timeout-time = 10s
 
      		stash-capacity = -1
    	}
    	
    	mailbox {
		#	mailbox-type = "akka.dispatch.BoundedControlAwareMailbox"
      		requirements {
       		#	"akka.dispatch.UnboundedMessageQueueSemantics" = akka.actor.mailbox.unbounded-queue-based
        	#	"akka.dispatch.BoundedMessageQueueSemantics" = akka.actor.mailbox.bounded-queue-based
        	#	"akka.dispatch.DequeBasedMessageQueueSemantics" = akka.actor.mailbox.unbounded-deque-based
        	#	"akka.dispatch.UnboundedDequeBasedMessageQueueSemantics" = akka.actor.mailbox.unbounded-deque-based
        	#	"akka.dispatch.BoundedDequeBasedMessageQueueSemantics" = akka.actor.mailbox.bounded-deque-based
        	#	"akka.dispatch.MultipleConsumerSemantics" = akka.actor.mailbox.unbounded-queue-based
        	#	"akka.dispatch.ControlAwareMessageQueueSemantics" = akka.actor.mailbox.unbounded-control-aware-queue-based
        	#	"akka.dispatch.UnboundedControlAwareMessageQueueSemantics" = akka.actor.mailbox.unbounded-control-aware-queue-based
        	#	"akka.dispatch.BoundedControlAwareMessageQueueSemantics" = akka.actor.mailbox.bounded-control-aware-queue-based
        	#	"akka.event.LoggerMessageQueueSemantics" = akka.actor.mailbox.logger-queue
      		}
 		
      	#	unbounded-queue-based {
        #		mailbox-type = "akka.dispatch.UnboundedMailbox"
      	#	}
 		
      	#	bounded-queue-based {
        #		mailbox-type = "akka.dispatch.BoundedMailbox"
      	#	}
		
      	#	unbounded-deque-based {
        #		mailbox-type = "akka.dispatch.UnboundedDequeBasedMailbox"
      	#	}
      		
      	#	bounded-deque-based {
        #		mailbox-type = "akka.dispatch.BoundedDequeBasedMailbox"
      	#	}
		
      	#	unbounded-control-aware-queue-based {
        #		mailbox-type = "akka.dispatch.UnboundedControlAwareMailbox"
      	#	}
		
      	#	bounded-control-aware-queue-based {
        #		mailbox-type = "akka.dispatch.BoundedControlAwareMailbox"
      	#	}
		
      	#	logger-queue {
        #		mailbox-type = "akka.event.LoggerMailboxType"
      	#	}
    	}
    	
		
		worker-dispatcher {
  			type = "Dispatcher"
  			mailbox-capacity = 3000
  			mailbox-push-timeout-time = 120s
		}
			
		default-dispatcher {
		
			type = "Dispatcher"
			
			throughput = 10
			
			executor = "default-executor"
			
			default-executor {
        		fallback = "fork-join-executor"
      		}
      		
      		fork-join-executor {
      		
        		parallelism-min = 8

        		parallelism-factor = 3.0

        		parallelism-max = 64
 
        		task-peeking-mode = "FIFO"
      		}
      		
      		shutdown-timeout = 1s

      		throughput = 5

      		throughput-deadline-time = 0ms
 
      		attempt-teamwork = on
 
      		mailbox-requirement = ""
		}
		
		router.type-mapping {
      		from-code = "akka.routing.NoRouter"
      	}
      
		deployment {
			default {
					dispatcher = ""
        			mailbox = ""
					virtual-nodes-factor = 10
					router = "from-code"	
			}
				
      		serializers {
        		java = "akka.serialization.JavaSerializer"
        		proto = "akka.remote.serialization.ProtobufSerializer"
      		}
 
      		serialization-bindings {
        		"java.lang.String" = java
        		"com.google.protobuf.Message" = proto
      		}
		}
	
		debug {
      		receive = off
      		autoreceive = off
      		lifecycle = off
      		fsm = off
      		event-stream = off
      		unhandled = off
      		router-misconfiguration = off
    	}
    	
    	scheduler {
    		tick-duration = 10ms
    		implementation = akka.actor.LightArrayRevolverScheduler
    		shutdown-timeout = 5s
    	}
	}
	
	scheduler {
      	tick-duration = 33ms
      	ticks-per-wheel = 512
      	implementation = akka.actor.LightArrayRevolverScheduler
      	shutdown-timeout = 5s
    }
	
	remote {
	 	transport = "akka.remote.netty.NettyRemoteTransport"
	 	
	 	netty.tcp {
	  	    hostname = "0.0.0.0"
	  	    port = 0
	  	    maximum-frame-size = 99999999999b
	  	}

		transport-failure-detector {
    		heartbeat-interval = 120 s
    		acceptable-heartbeat-pause = 60 s
  		}
	}

	cluster {
	 	failure-detector {
      		threshold = 12
      		acceptable-heartbeat-pause = 120s
      		heartbeat-interval = 5s
      		heartbeat-request {
        		expected-response-after = 120s
      		}
    }
		jmx.enabled = on
		auto-down-unreachable-after = off
        roles = ["master"]
	}
}